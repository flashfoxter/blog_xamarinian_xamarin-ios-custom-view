﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using System.ComponentModel;
using System.CodeDom.Compiler;

namespace TutCustomViewClass
{
	[Register("CustomViewClass"), DesignTimeVisible(true)]
	public class CustomViewClass : UIView
	{

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView tabIcon { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel[] tabs { get; set; }


		public CustomViewClass ()
		{
			Initialize ();
		}

		public CustomViewClass (IntPtr handle) : base (handle) {
			Initialize ();
		}

		void Initialize()
		{

		}

		public override void AwakeFromNib ()
		{
			for (int i = 0; i < tabs.Length; i++) {
				tabs [i].Text = "Tab " + i;
			}


			base.AwakeFromNib ();
		}

		public override void Draw(CGRect rect)
		{

		}


	}
}

